package de.ur.iw.lsl_monitor;

import java.io.IOException;
import java.util.concurrent.BlockingQueue;

import edu.ucsd.sccn.LSL;
import edu.ucsd.sccn.LSL.StreamInfo;
import edu.ucsd.sccn.LSL.XMLElement;
import javafx.animation.AnimationTimer;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart.Data;
import javafx.scene.chart.XYChart.Series;
import javafx.scene.image.Image;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class ChartWindowController {

	private static final String PREFERENCES_KEY_WINDOW_X = "windowX";
	private static final String PREFERENCES_KEY_WINDOW_Y = "windowY";
	private static final String PREFERENCES_KEY_WINDOW_WIDTH = "windowWidth";
	private static final String PREFERENCES_KEY_WINDOW_HEIGHT = "windowHeight";

	private Stage stage;
	private LineChart<Number, Number> lineChart;
	Series<Number, Number> series = new Series<>();

	private final double timeWindowSize;
	private final StreamDataReader reader;

	public ChartWindowController(StreamDataReader reader, double timeWindowSize) throws IOException {
		this.timeWindowSize = timeWindowSize;
		this.reader = reader;

		stage = createWindow(StageStyle.DECORATED);
		stage.show();

		if (Screen.getScreensForRectangle(stage.getX(), stage.getY(), stage.getWidth(), stage.getHeight()).isEmpty()) {
			stage.centerOnScreen();
		}

		new AnimationTimer() {
			private long lastUpdateLineChart = 0;
			private long lastUpdateTitle = 0;

			@Override
			public void handle(long now) {
				// make sure we update the display with "only" 100Hz
				if (lastUpdateLineChart + 10000000l < now) {
					lastUpdateLineChart = now;
					updateLineChart();
				}

				if (lastUpdateTitle + 1000000000l < now) {
					lastUpdateTitle = now;
					updateTitle();
				}
			}
		}.start();
	}

	private Stage createWindow(StageStyle stageStyle) {
		Stage stage = new Stage();

		try {
			stage.getIcons().add(new Image(FileHelper.getResourceAsStream("api-lsl.png")));
		} catch (IOException e) {
			throw new RuntimeException(e);
		}

		NumberAxis xAxis = new NumberAxis();
		NumberAxis yAxis = new NumberAxis();
		lineChart = new LineChart<>(xAxis, yAxis);
		xAxis.setAutoRanging(false);
		xAxis.setTickUnit(5.0);
		yAxis.setAutoRanging(true);
		yAxis.setForceZeroInRange(false);
		lineChart.setCreateSymbols(false);
		lineChart.setAnimated(false);
		lineChart.getData().add(series);
		stage.setScene(new Scene(lineChart));

		stage.getScene().getStylesheets().add("stylesheet.css");
		stage.initStyle(stageStyle);

		stage.setX(MainClass.preferences().getDouble(PREFERENCES_KEY_WINDOW_X, stage.getX()));
		stage.setY(MainClass.preferences().getDouble(PREFERENCES_KEY_WINDOW_Y, stage.getY()));
		stage.setWidth(MainClass.preferences().getDouble(PREFERENCES_KEY_WINDOW_WIDTH, stage.getWidth()));
		stage.setHeight(MainClass.preferences().getDouble(PREFERENCES_KEY_WINDOW_HEIGHT, stage.getHeight()));
		stage.xProperty().addListener((obs, oldVal, newVal) -> {
			if (!stage.isMaximized() && newVal.doubleValue() >= 0) {
				MainClass.preferences().putDouble(PREFERENCES_KEY_WINDOW_X, newVal.doubleValue());
			}
		});
		stage.yProperty().addListener((obs, oldVal, newVal) -> {
			if (!stage.isMaximized() && newVal.doubleValue() >= 0) {
				MainClass.preferences().putDouble(PREFERENCES_KEY_WINDOW_Y, newVal.doubleValue());
			}
		});
		stage.widthProperty().addListener((obs, oldVal, newVal) -> {
			if (!stage.isMaximized() && newVal.doubleValue() >= 0) {
				MainClass.preferences().putDouble(PREFERENCES_KEY_WINDOW_WIDTH, newVal.doubleValue());
			}
		});
		stage.heightProperty().addListener((obs, oldVal, newVal) -> {
			if (!stage.isMaximized() && newVal.doubleValue() >= 0) {
				MainClass.preferences().putDouble(PREFERENCES_KEY_WINDOW_HEIGHT, newVal.doubleValue());
			}
		});

		return stage;
	}

	private void updateLineChart() {
		double localClock = LSL.local_clock();
		double oldestClockValue = localClock - timeWindowSize;

		NumberAxis xAxis = (NumberAxis) lineChart.getXAxis();
		xAxis.setLowerBound(oldestClockValue);
		xAxis.setUpperBound(localClock);

		ObservableList<Data<Number, Number>> data = series.getData();
		data.removeIf(x -> x.getXValue().doubleValue() < oldestClockValue);

		BlockingQueue<Data<Number, Number>> dataQueue = reader.getDataQueue();

		final double timePerDisplayedDataPoint = (timeWindowSize / (xAxis.getWidth() + 1)) / 5;
		while (!dataQueue.isEmpty()) {
			Data<Number, Number> dataPoint = dataQueue.poll();
			if (data.isEmpty()) {
				dataPoint.setExtraValue(new Integer(1));
				data.add(dataPoint);
			} else {
				Data<Number, Number> lastDisplayedDataPoint = data.get(data.size() - 1);
				double lastX = lastDisplayedDataPoint.getXValue().doubleValue();
				double thisX = dataPoint.getXValue().doubleValue();
				if (lastX + timePerDisplayedDataPoint < thisX) {
					dataPoint.setExtraValue(new Integer(1));
					data.add(dataPoint);
				} else {
					int aggregatedSamples = (int) lastDisplayedDataPoint.getExtraValue();
					double lastY = lastDisplayedDataPoint.getYValue().doubleValue();
					double thisY = dataPoint.getYValue().doubleValue();
					double x = (lastX * aggregatedSamples + thisX * 1) / aggregatedSamples + 1;
					double y = (lastY * aggregatedSamples + thisY * 1) / aggregatedSamples + 1;
					dataPoint.setXValue(x);
					dataPoint.setYValue(y);
					dataPoint.setExtraValue(aggregatedSamples + 1);
				}
			}
		}
	}

	private void updateTitle() {
		String title = "pred: " + reader.getPredicate() + " - ch: " + reader.getChannel();
		if (reader.getStreamInfo() != null) {
			StreamInfo info = reader.getStreamInfo();
			title += " --- " + info.name();
			title += " - " + info.type();
			title += " - " + info.nominal_srate() + " Hz";
			try {
				XMLElement channelXML = info.desc().child("channels").first_child();
				for (int i = 0; i < reader.getChannel(); i++) {
					channelXML = channelXML.next_sibling();
				}
				title += " - " + channelXML.child_value("label");
			} catch (Exception e) {
				
			}
		} else {
			title += " - NO STREAM";
		}
		if (!title.equals(stage.getTitle())) {
			stage.setTitle(title);
		}
	}
}
