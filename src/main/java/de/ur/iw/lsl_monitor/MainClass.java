package de.ur.iw.lsl_monitor;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.prefs.Preferences;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import javafx.application.Application;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class MainClass {

	static Options options = new Options();
	static String preferencesPrefix = null;

	static {
		Option predicate = new Option("p", "predicate", true,
				"XPath 1.0 Predicate for the <info> node of resolved streams.");
		predicate.setRequired(true);
		options.addOption(predicate);

		Option channel = new Option("c", "channel", true, "Channel index within stream, begins with 0.");
		channel.setRequired(false);
		options.addOption(channel);

		Option timeWindowSize = new Option("w", "timewindow", true, "Size of the displayed time window in seconds.");
		timeWindowSize.setRequired(false);
		options.addOption(timeWindowSize);

		Option ignoreTimeCorrection = new Option("t", "ignoretimecorrection", true,
				"Can be used to ignore LSL's time_correction mechanism and assume that data is from now.");
		ignoreTimeCorrection.setRequired(false);
		options.addOption(ignoreTimeCorrection);
	}

	/**
	 * @return the <code>Preferences</code> instance representing this program.
	 */
	public static Preferences preferences() {
		if (preferencesPrefix == null) {
			return Preferences.userNodeForPackage(MainClass.class);
		} else {
			return Preferences.userNodeForPackage(MainClass.class).node(preferencesPrefix);
		}
	}

	public static void main(String[] args) {
		Application.launch(JavaFXMain.class, args);
	}

}
