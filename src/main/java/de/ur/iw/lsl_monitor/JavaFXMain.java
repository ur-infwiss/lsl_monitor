package de.ur.iw.lsl_monitor;

import javafx.application.Application;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.ParseException;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

public class JavaFXMain extends Application {

    @Override
    public void start(Stage primaryStage) {
        try {
            CommandLine cmd = new DefaultParser().parse(MainClass.options, this.getParameters().getRaw().toArray(new String[0]));
            String predicate = cmd.getOptionValue("p");
            int channel = Integer.parseInt(cmd.getOptionValue("c", "0"));
            double timeWindowSize = Double.parseDouble(cmd.getOptionValue("w", "30"));
            boolean ignoreTimeCorrection = Boolean.parseBoolean(cmd.getOptionValue("t", "False"));

            MainClass.preferencesPrefix = "predicate: " + predicate.replace('/', '#') + " channel: " + channel;

            StreamDataReader reader = new StreamDataReader(predicate, channel, ignoreTimeCorrection);
            new ChartWindowController(reader, timeWindowSize);

            Thread readerThread = new Thread(reader);
            readerThread.setDaemon(true);
            readerThread.start();

        } catch (ParseException | IOException e) {
            showErrorDialog(e);
        }

    }

    private static void showErrorDialog(Exception e) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setHeaderText(e.getLocalizedMessage());

        HelpFormatter formatter = new HelpFormatter();
        String cmdLineSyntax = "<program-name> <options>";
        formatter.printHelp(cmdLineSyntax, MainClass.options);

        StringWriter stringWriter = new StringWriter();
        formatter.printHelp(new PrintWriter(stringWriter), 95, cmdLineSyntax, "", MainClass.options, 0, 3, "");
        String text = stringWriter.toString();

        Label textLabel = new Label(text);
        textLabel.setWrapText(false);
        textLabel.setFont(Font.font(java.awt.Font.MONOSPACED));
        alert.getDialogPane().setContent(textLabel);
        alert.setResizable(true);

        StringWriter stringWriterStracktrace = new StringWriter();
        e.printStackTrace(new PrintWriter(stringWriterStracktrace));
        alert.getDialogPane().setExpandableContent(new TextArea(stringWriterStracktrace.toString()));

        alert.showAndWait();
    }

}
